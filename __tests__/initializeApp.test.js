/**
 * @jest-environment jsdom
 */

import { initializeApp } from '../src/scripts/utils';
import { test, expect } from '@jest/globals';

test('Вызов initializeApp должен добавлять отрендеренный element в потомков родительского элемента', () => {
  const element = { tag: 'div', child: 'Hello world string!' };
  const root = document.createElement('div');
  initializeApp(root, element);

  const childrenCount = root.children.length;
  expect(childrenCount).toBe(1);

  const elementNode = root.children.item(0);
  expect(elementNode.tagName.toLowerCase()).toBe(element.tag.toLowerCase());

  const elementInnerHTML = elementNode.innerHTML;
  expect(elementInnerHTML).toBe(element.child);
});
